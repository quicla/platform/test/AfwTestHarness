/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.testtype;

import com.android.afwtest.tradefed.TestConfig;
import com.android.compatibility.common.tradefed.build.CompatibilityBuildHelper;
import com.android.compatibility.common.tradefed.testtype.IModuleRepo;
import com.android.compatibility.common.tradefed.testtype.CompatibilityTest;
import com.android.compatibility.common.tradefed.testtype.ModuleRepo;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.util.FileUtil;

import java.io.File;
import java.io.IOException;

/**
 * Customized {@link CompatibilityTest} for running afw-test tests.
 * <p>Supports running all the tests contained in an afw-test plan, or individual test packages.
 * </p>
 */
@OptionClass(alias = "compatibility")
public class AfwTest extends CompatibilityTest {

    private static final String CMD_DISABLE_PKG_VERIFICATION =
            "settings put global package_verifier_enable 0";

    @Option(name = "afw-test-config-file",
            description = "Test harness config file to replace afw-test.props.")
    private String mConfigFile = null;

    @Option(name = "screenshot",
            description = "Take a screenshot on every test")
    private boolean mScreenshot = false;

    /**
     * A {@link CompatibilityBuildHelper} instance to access afw-test build info, such as the
     * absolute paths of the test plan file and test case apks.
     */
    protected CompatibilityBuildHelper mAfwTestBuild = null;

    /**
     * Internal {@link ITestInvocationListener} wrapping one provided by framework with additional
     * listeners specific for {@link AfwTest}.
     */
    protected ITestInvocationListener mInternalListener = null;

    /**
     * A {@link ITestDevice} instance representing a device under test.
     */
    protected ITestDevice mDevice = null;

    /**
     * {@inheritDoc}
     */
    public AfwTest() {
        this(1 /*totalShards*/, new ModuleRepo() /*moduleRepo*/, 0 /*shardIndex*/);
    }

    /**
     * {@inheritDoc}
     */
    public AfwTest(int totalShards, IModuleRepo moduleRepo, Integer shardIndex) {
        super(totalShards, moduleRepo, shardIndex);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBuild(IBuildInfo build) {
        super.setBuild(build);

        mAfwTestBuild = new CompatibilityBuildHelper(build);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        mInternalListener = listener;

        beforeTest();
        super.run(mInternalListener);
        afterTest();
    }

    /**
     * Executes general preparation steps before running the test.
     *
     * @throws DeviceNotAvailableException if connection to the device is lost.
     */
    protected void beforeTest() throws DeviceNotAvailableException {
        CLog.i("Running afw-test");

        mDevice = getDevice();
        if (mDevice == null) {
            throw new IllegalArgumentException("Device not found");
        }

        try {
            // Replace afw-test.props with a customized file if there is.
            if (mConfigFile != null) {
                FileUtil.copyFile(new File(mConfigFile), getTestConfigFile());
            }

            // Init TestConfig singleton.
            TestConfig.init(getTestConfigFile());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Disable package verification.
        // This is necessary because CtsTest.run() will push TestDeviceSetup.apk
        // to the testing device which will trigger package verification dialog.
        mDevice.executeShellCommand(CMD_DISABLE_PKG_VERIFICATION);

        if (mScreenshot) {
            mInternalListener = new ScreenshotListener(mInternalListener, mDevice);
        }
    }

    /**
     * Executes general tear down steps after running the test.
     *
     * @throws DeviceNotAvailableException if connection to the device is lost.
     */
    protected void afterTest() throws DeviceNotAvailableException {
        // Disable package verification again; it might be enabled by some tests.
        mDevice.executeShellCommand(CMD_DISABLE_PKG_VERIFICATION);
    }

    /**
     * Reads test configuration file, afw-test.props.
     *
     * @return {@link File} object keeping loaded content from afw-test.props
     */
    protected File getTestConfigFile() throws IOException {
        return new File(mAfwTestBuild.getTestsDir(), "afw-test.props");
    }
}
