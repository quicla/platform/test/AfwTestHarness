/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics;

import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.BuildFingerprint;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.DeviceInfo;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.InvocationMetric;

import com.android.afwtest.tradefed.utils.ReflectionUtils;
import com.android.compatibility.common.tradefed.build.CompatibilityBuildHelper;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.BuildError;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;
import com.google.protobuf.nano.MessageNano;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import javax.annotation.concurrent.GuardedBy;

/** {@link ITargetCleaner} to collect metrics during test invocation. */
public class AfwTestMetricsCollectorTargetPreparer implements ITargetCleaner {

    @Option(
        name = "collector-class-names",
        description = "Class names of metrics collectors to use."
    )
    private final List<String> mCollectorClassNames = new ArrayList<>();

    // List of metrics collectors.
    private final List<MetricsCollector> mCollectors = new ArrayList<>();

    // Common list of collected metrics.
    @GuardedBy("mMetricsLock")
    private final List<InvocationMetric> mMetrics = new ArrayList<>();

    // Lock to synchronize access to common list of collected metrics
    // from multiple metrics collectors.
    // Lock is used instead of synchronized collections to ensure fairness of the access.
    private final ReadWriteLock mMetricsLock = new ReentrantReadWriteLock(true);

    /** {@inheritDoc} */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, BuildError, DeviceNotAvailableException {
        if (!isCollectionEnabled()) {
            return;
        }

        try {
            createCollectors();
            for (MetricsCollector collector : mCollectors) {
                collector.init(device, buildInfo, this::addMetricToList);
            }
        } catch (MetricsCollectorException e) {
            throw new TargetSetupError(
                    "Exception was thrown during metrics collectors initialization", e, null);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {
        if (!isCollectionEnabled()) {
            return;
        }

        for (MetricsCollector collector : mCollectors) {
            collector.destroy();
        }
        try {
            writeMetrics(device, buildInfo);
        } catch (MetricsCollectorException ex) {
            CLog.e(ex);
        }
    }

    /**
     * Checks if metrics collection is enabled for current invocation.
     *
     * @return {@code true} if metrics collection is enabled, {@code false} otherwise.
     */
    protected boolean isCollectionEnabled() {
        return true;
    }

    /**
     * {@link Consumer} passed to collectors to ensure synchronized access to the list of collected
     * metrics.
     *
     * @param metric metric to add to the list of collected metrics.
     */
    private void addMetricToList(InvocationMetric metric) {
        mMetricsLock.writeLock().lock();
        mMetrics.add(metric);
        mMetricsLock.writeLock().unlock();
    }

    /**
     * Collects information about the device on which invocation happened.
     *
     * @param device current {@link ITestDevice}.
     * @param buildInfo current {@link IBuildInfo}.
     * @return information about the device on which invocation happened.
     */
    private DeviceInfo getDeviceInfo(ITestDevice device, IBuildInfo buildInfo) {
        final Map<String, String> buildAttrs = buildInfo.getBuildAttributes();
        BuildFingerprint buildFingerprint = new BuildFingerprint();
        buildFingerprint.productBrand = buildAttrs.get("cts:build_brand");
        buildFingerprint.productName = buildAttrs.get("cts:build_product");
        buildFingerprint.productDevice = buildAttrs.get("cts:build_device");
        buildFingerprint.platformVersion = buildAttrs.get("cts:build_version_release");
        buildFingerprint.release = buildAttrs.get("cts:build_id");
        buildFingerprint.versionIncremental = buildAttrs.get("cts:build_version_incremental");
        buildFingerprint.type = buildAttrs.get("cts:build_type");
        buildFingerprint.tags = buildAttrs.get("cts:build_tags");

        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.buildFingerprint = buildFingerprint;
        deviceInfo.apiLevel = Long.parseLong(buildAttrs.get("cts:build_version_sdk"));
        try {
            deviceInfo.gmsVersion =
                    device.getAppPackageInfo("com.google.android.gms").getVersionName();
        } catch (DeviceNotAvailableException e) {
            CLog.e(e);
            deviceInfo.gmsVersion = "N/A";
        }

        return deviceInfo;
    }

    /**
     * Writes collected metrics to file.
     *
     * @param buildInfo current {@link IBuildInfo}.
     * @throws MetricsCollectorException if any error has occurred upon writing collected metrics.
     */
    private void writeMetrics(ITestDevice device, IBuildInfo buildInfo)
            throws MetricsCollectorException {
        CompatibilityBuildHelper buildHelper = new CompatibilityBuildHelper(buildInfo);
        FileOutputStream os = null;
        try {
            InvocationMetrics metrics = new InvocationMetrics();
            metrics.id = UUID.randomUUID().toString();
            metrics.timestampMillis = System.currentTimeMillis();
            metrics.deviceInfo = getDeviceInfo(device, buildInfo);

            mMetricsLock.readLock().lock();
            metrics.metrics = mMetrics.toArray(InvocationMetric.emptyArray());
            mMetricsLock.readLock().unlock();

            if (metrics.metrics.length == 0) {
                return;
            }

            File logsDir = buildHelper.getLogsDir();
            // createTempFile generates unique file name.
            File resultFile = File.createTempFile("provisioning_metrics_", ".pbuf", logsDir);
            os = new FileOutputStream(resultFile);
            os.write(MessageNano.toByteArray(metrics));
        } catch (IOException e) {
            throw new MetricsCollectorException("Cannot save collected metrics.", e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    /**
     * Creates instances of metrics collectors whose class names are specified in
     * 'collector-class-names' option.
     *
     * @throws MetricsCollectorException if an exception occurs during collectors instantiation.
     */
    private void createCollectors() throws MetricsCollectorException {
        for (String className : mCollectorClassNames) {
            try {
                mCollectors.add(ReflectionUtils.getInstanceOf(className, MetricsCollector.class));
            } catch (ReflectiveOperationException e) {
                throw new MetricsCollectorException(e);
            }
        }
    }
}
