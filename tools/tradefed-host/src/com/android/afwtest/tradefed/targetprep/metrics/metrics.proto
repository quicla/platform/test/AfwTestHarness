// Copyright (C) 2017 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto2";

option java_package = "com.android.afwtest.tradefed.targetprep.metrics";

package com_android_afwtest_tradefed_targetprep_metrics;

// Message representing Android Build Fingerprint.
// google/bullhead/bullhead:7.1.1/N4F26O/3582057:user/release-keys
message BuildFingerprint {
  // google
  required string product_brand = 1;
  // bullhead
  required string product_name = 2;
  // bullhead
  required string product_device = 3;
  // 7.1.1
  required string platform_version = 4;
  // N4F26O
  required string release = 5;
  // 3582057
  required string version_incremental = 6;
  // user
  required string type = 7;
  // release-keys
  optional string tags = 8;
}

// Message describing device on which invocation happened.
message DeviceInfo {
  // Fingerprint of the build on the device.
  required BuildFingerprint build_fingerprint = 1;

  // Version of Google Play Services installed on the device.
  required string gms_version = 2;

  // API level of the device.
  required int64 api_level = 3;
}

// Message describing collection of metrics collected during an invocation.
message InvocationMetrics {

  // Enum of supported metric types.
  enum MetricType {
    UNKNOWN = 1;
    TOTAL_PROVISIONING_TIME_MS = 2;
    TOTAL_PSS_BY_USER = 3;
    PSS_BY_PROCESS = 4;
  }

  // Value of metric.
  message MetricValue {
    oneof value {
      uint64 uint_value = 1;
      sint64 sint_value = 2;
      string string_value = 3;
    }
  }

  // Wrapper object to bypass limitation of not being able to have map inside
  // oneof.
  message MetricValueMap {
    map<string, MetricValue> values = 1;
  }

  // Message describing a single metric collected during an invocation.
  message InvocationMetric {
    required MetricType metric_type = 1;
    oneof metric_value {
      MetricValue scalar_value = 2;
      MetricValueMap map_value = 3;
    }
  }

  // Unique ID.
  required string id = 1;

  // Timestamp of the invocation in milliseconds.
  required int64 timestamp_millis = 2;

  // Information about the device on which invocation happened.
  required DeviceInfo device_info = 3;

  // Collection of metrics.
  repeated InvocationMetric metrics = 4;
}