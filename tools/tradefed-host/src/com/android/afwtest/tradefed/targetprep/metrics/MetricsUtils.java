/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics;

import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.MetricValueMap;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.MetricValue;

/**
 * Utility class containing methods related to metrics collection classes.
 */
public final class MetricsUtils {

    /** Prevent class from instantiation. */
    private MetricsUtils() {
    }

    /**
     * Creates an instance of {@link MetricValueMap.ValuesEntry} from given key and value.
     *
     * @param key key of the entry to add.
     * @param value value of the entry to add.
     * @return created instance of {@link MetricValueMap.ValuesEntry}.
     */
    public static MetricValueMap.ValuesEntry toMetricValueMapEntry(String key, long value) {
        MetricValue metricValue = new MetricValue();
        metricValue.setUintValue(value);

        MetricValueMap.ValuesEntry valuesEntry = new MetricValueMap.ValuesEntry();
        valuesEntry.key = key;
        valuesEntry.value = metricValue;

        return valuesEntry;
    }
}
