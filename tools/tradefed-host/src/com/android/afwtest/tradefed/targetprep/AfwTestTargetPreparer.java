/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.afwtest.tradefed.TestConfig;
import com.android.compatibility.common.tradefed.build.CompatibilityBuildHelper;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.device.PackageInfo;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.TargetSetupError;
import com.android.tradefed.util.CommandResult;
import com.android.tradefed.util.CommandStatus;
import com.android.tradefed.util.RunUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;


/**
 * Abstract class to keep common functionality.
 */
public abstract class AfwTestTargetPreparer {

    private CompatibilityBuildHelper mBuildHelper;
    private Long mTimeoutSecs = TimeUnit.MINUTES.toSeconds(5);

    /**
     * Gets an instance of {@link CompatibilityBuildHelper}.
     *
     * @param buildInfo reference to {@link IBuildInfo}
     * @return reference to {@link CompatibilityBuildHelper} instance
     */
    protected CompatibilityBuildHelper getCtsBuildHelper(IBuildInfo buildInfo) {
        if (mBuildHelper == null) {
            mBuildHelper = new CompatibilityBuildHelper(buildInfo);
        }
        return mBuildHelper;
    }

    /**
     * Gets repository directory.
     *
     * @param buildInfo reference to {@link IBuildInfo}
     * @return File path to repository folder.
     */
    protected File getRepositoryDir(IBuildInfo buildInfo) throws FileNotFoundException {
        return getCtsBuildHelper(buildInfo).getDir();
    }

    /**
     * Gets apk file from testcases dir.
     *
     * @param buildInfo reference to {@link IBuildInfo}
     * @param fileName apk file name
     * @return {@link File} of the apk
     */
    protected File getApk(IBuildInfo buildInfo, String fileName) throws TargetSetupError {
        try {
            return new File(getCtsBuildHelper(buildInfo).getTestsDir(), fileName);
        } catch (FileNotFoundException e) {
            throw new TargetSetupError(String.format("Couldn't get apk: %s", fileName), e);
        }
    }

    /**
     * Enable adb root.
     *
     * @param device test device
     * @throws TargetSetupError if failed to enable adb root
     * @throws DeviceNotAvailableException if test device becomes unavailable
     */
    protected void enableAdbRoot(ITestDevice device)
            throws TargetSetupError, DeviceNotAvailableException {
        // Enable adb root
        if (!device.enableAdbRoot()) {
            throw new TargetSetupError(
                    String.format("Failed to enable adb root: %s", device.getSerialNumber()));
        }
    }

    /**
     * Waits and gets certain app package info.
     *
     * @param device test device
     * @param pkgName app package name
     * @param timeoutMs timeout in ms
     * @return App package info of given package if found, null otherwise
     * @throws DeviceNotAvailableException if test device becomes unavailable
     */
    protected PackageInfo waitForAppPkgInfo(ITestDevice device, String pkgName, long timeoutMs)
            throws DeviceNotAvailableException {

        long threadSleepTimeMs = TimeUnit.SECONDS.toMillis(5);

        PackageInfo pkgInfo = device.getAppPackageInfo(pkgName);

        while (pkgInfo == null && timeoutMs > 0) {
            RunUtil.getDefault().sleep(threadSleepTimeMs);
            timeoutMs -= threadSleepTimeMs;

            pkgInfo = device.getAppPackageInfo(pkgName);
        }

        return pkgInfo;
    }

    /**
     * Gets {@link TestConfig} instance.
     *
     * @return {@link TestConfig} instance or null if not found;
     */
    protected TestConfig getTestConfig() {
        return TestConfig.getInstance();
    }

    /**
     * Gets timeout size from config file.
     *
     * @return timeout size in integer
     */
    protected int getTimeoutSize() {
        TestConfig testConfig = TestConfig.getInstance();

        // Target prepares should still work without test config file;
        if (testConfig == null) {
            return 1;
        }

        return TestConfig.getInstance().getTimeoutSize();
    }

    /**
     * Wipes device via fastboot.
     *
     * <p>
     * Refers to com.android.tradefed.targetprep.DeviceWiper
     * </p>
     *
     * @param device test device
     */
    protected void wipeDevice(ITestDevice device)
            throws DeviceNotAvailableException, TargetSetupError {

        CLog.i(String.format("Wiping device %s".format(device.getSerialNumber())));

        device.rebootIntoBootloader();
        fastbootFormat(device, "cache");
        fastbootFormat(device, "userdata");
        device.executeFastbootCommand("reboot");
    }


    /**
     * Performs fastboot erase/format operation on certain partition
     *
     * @param device test device
     * @param partition android partition, e.g. userdata, cache, system
     */
    protected void fastbootFormat(ITestDevice device, String partition)
            throws DeviceNotAvailableException, TargetSetupError {

        CLog.i(String.format("Attempting: fastboot format %s", partition));
        CommandResult r = device.executeLongFastbootCommand("format", partition);
        if (r.getStatus() != CommandStatus.SUCCESS) {
            throw new TargetSetupError(
                    String.format("format %s failed: %s", partition, r.getStderr()));
        }
    }

    /**
     * "Soft" reboot the device by adb shell stop/start.
     *
     * @param device test device
     */
    protected void softReboot(ITestDevice device)
            throws DeviceNotAvailableException, TargetSetupError {

        CLog.i(String.format("Soft reboot device %s".format(device.getSerialNumber())));

        device.executeShellCommand("stop");
        device.executeShellCommand("start");
        device.waitForDeviceAvailable();
        // enableAdbRoot() will check if device.isEnableRoot()
        device.enableAdbRoot();
    }
}

