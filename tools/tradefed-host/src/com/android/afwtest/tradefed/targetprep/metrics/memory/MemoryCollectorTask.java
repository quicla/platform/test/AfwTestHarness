/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics.memory;

import static com.android.afwtest.tradefed.targetprep.metrics.MetricsUtils.toMetricValueMapEntry;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.InvocationMetric;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.MetricValueMap;

import com.android.afwtest.tradefed.utils.AdbShellUtils;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * {@link Runnable} of the task that is executed in equal time intervals and collects memory
 * usage information.
 */
public class MemoryCollectorTask implements Runnable {

    /**
     * {@link MemoryInfoGrouper} collection to be used to emit metrics.
     */
    private Collection<MemoryInfoGrouper> mMemoryInfoGroupers;

    /**
     * Current {@link ITestDevice}.
     */
    private ITestDevice mTestDevice;

    /**
     * {@link Consumer} of collected information.
     */
    private Consumer<InvocationMetric> mConsumer;

    /**
     * Tick counter.
     */
    private Supplier<Long> mTickCounter;

    /**
     * Constructs a new instance of the task.
     *
     * @param testDevice current {@link ITestDevice}.
     * @param consumer {@link Consumer} of collected information.
     * @param tickCounter tick counter.
     * @param groupers {@link MemoryInfoGrouper} collection to be used to emit metrics.
     */
    public MemoryCollectorTask(ITestDevice testDevice,
            Consumer<InvocationMetric> consumer,
            Supplier<Long> tickCounter,
            Collection<MemoryInfoGrouper> groupers) {
        mTestDevice = testDevice;
        mConsumer = consumer;
        mTickCounter = tickCounter;
        mMemoryInfoGroupers = groupers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        try {
            Set<AdbShellUtils.ProcessInfo> ps = AdbShellUtils.getPS(mTestDevice);
            Map<Long, Long> memoryInfo = AdbShellUtils.getMemoryInfo(mTestDevice);
            MetricValueMap.ValuesEntry tickEntry =
                    toMetricValueMapEntry("tick", mTickCounter.get());

            mMemoryInfoGroupers
                    .stream()
                    .map(grouper -> {
                        Collection<MetricValueMap.ValuesEntry> entries =
                                grouper.group(ps, memoryInfo);
                        entries.add(tickEntry);

                        MetricValueMap.ValuesEntry[] entriesArray =
                                entries.toArray(new MetricValueMap.ValuesEntry[entries.size()]);

                        MetricValueMap mapValue = new MetricValueMap();
                        mapValue.values = entriesArray;

                        InvocationMetric metric = new InvocationMetric();
                        metric.metricType = grouper.getMetricType();
                        metric.setMapValue(mapValue);

                        return metric;
                    }).forEach(mConsumer);

        } catch (DeviceNotAvailableException ignore) {}
    }
}
