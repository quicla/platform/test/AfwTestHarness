#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Test packages that require an associated test package XML.
afw_th_test_packages := \
    AfwTestNfcProvisioningTestCases \
    AfwTestNonSuwPoProvisioningTestCases \
    AfwTestQRCodeProvisioningTestCases \
    AfwTestSuwDoProvisioningTestCases \
    AfwTestSuwPoProvisioningTestCases

# Support packages
afw_th_support_packages := \
    AfwThDeviceAdmin \
    AfwThSystemUtil \
    AfwThUtil

# Additional prebuilt utilities used by tests
afw_th_prebuilt_utils := \
    CtsDeviceInfo.apk \
    TestDpc.apk

afw_th_host_libraries :=


# All the test case apk files that will end up under the repository/testcases
# directory of the final AfW Test distribution.
AFW_TH_TEST_CASES := $(call afw-th-get-package-paths,$(afw_th_test_packages)) \
    $(call afw-th-get-lib-paths,$(afw_th_host_libraries))

# All the support apk files that will end up under repository/testcases
# directory of the final AfW Test distribution.
AFW_TH_SUPPORT_PACKAGE_APKS := $(call afw-th-get-package-paths,$(afw_th_support_packages))

# All the XMLs that will end up under the repository/testcases
# and that need to be created before making the final AfW Test Harness distribution.
AFW_TH_TEST_CONFIGS := $(call afw-th-get-test-configs,$(afw_th_test_packages)) \
    $(call afw-th-get-test-configs,$(afw_th_host_libraries))

# The following files will be placed in the tools directory of the Harness distribution
AFW_TH_TOOLS_LIST :=

