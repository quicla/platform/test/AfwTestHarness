/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.qrcodeprovisioning;

import static com.android.afwtest.common.test.TestConfig.DEFAULT_TEST_CONFIG_FILE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.os.SystemClock;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;

import com.android.afwtest.common.qrcodeprovisioning.Utils;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.provisioning.AutomationDriver;
import com.android.afwtest.uiautomator.test.AbstractTestCase;
import com.android.afwtest.uiautomator.test.AfwTestUiWatcher;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * QR code provisioning test.
 */
@RunWith(AndroidJUnit4.class)
public class QRCodeProvisioningTest extends AbstractTestCase {

    private static final String SUW_PACKAGE_NAME = "com.google.android.setupwizard";
    private static final int TAP_COUNT = 6;
    private static final int TAP_INTERVAL = 300;

    private static final int QR_READER_INSTALL_TIMEOUT = 30000;
    private static final int QR_READER_INSTALL_ATTEMPTS = 6;

    private static final BySelector NEXT_BUTTON_SELECTOR =
        By.text(Pattern.compile("next", Pattern.CASE_INSENSITIVE)).clickable(true);
    private static final BySelector SWITCH_CAMERA_BUTTON_SELECTOR =
        By.res(SUW_PACKAGE_NAME, "switch_camera_button");
    private static final BySelector WELCOME_TITLE_SELECTOR =
        By.text(Pattern.compile("welcome|hi there", Pattern.CASE_INSENSITIVE));

    /**
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        AfwTestUiWatcher.register(getUiDevice());
    }

    /**
     * {@inheritDoc}
     */
    @After
    public void tearDown() throws Exception {
        AfwTestUiWatcher.unregister(getUiDevice());
    }

    /**
     * Test QR code provisioning flow.
     */
    @Test
    public void testQRCodeProvisioning() throws Exception {
        UiObject2 welcomeTitle = WidgetUtils.safeWait(getUiDevice(), WELCOME_TITLE_SELECTOR);
        for (int i = 0; i < TAP_COUNT; i++) {
            welcomeTitle.click();
            SystemClock.sleep(TAP_INTERVAL);
        }
        WidgetUtils.waitAndClick(getUiDevice(), NEXT_BUTTON_SELECTOR);
        UiObject2 frontCameraButton =
            WidgetUtils.safeWait(getUiDevice(), SWITCH_CAMERA_BUTTON_SELECTOR,
                QR_READER_INSTALL_TIMEOUT, QR_READER_INSTALL_ATTEMPTS);
        if (frontCameraButton == null) {
            throw new UiObjectNotFoundException("Cannot identify QR reader");
        }

        // Start provisioning
        String deviceAdminPkgName = Utils.startProvisioning(getContext(), DEFAULT_TEST_CONFIG_FILE);
        assertNotNull(deviceAdminPkgName);
        AutomationDriver driver = new AutomationDriver(getUiDevice());
        assertTrue("QR Code provisioning didn't finish",
                driver.runQRCodeProvisioning(TestConfig.getDefault()));
        DevicePolicyManager devicePolicyManager =
                (DevicePolicyManager) getContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
        // Verify if the device is provisioned.
        assertTrue("Provisioning failed", devicePolicyManager.isDeviceOwnerApp(deviceAdminPkgName));
    }
}
