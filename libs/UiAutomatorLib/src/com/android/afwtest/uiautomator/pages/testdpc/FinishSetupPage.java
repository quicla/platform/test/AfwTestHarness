/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.testdpc;

import static com.android.afwtest.uiautomator.Constants.STAT_TESTDPC_WORK_PROFILE_CREATION_TIME;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_NEXT_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_PKG_NAME;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.view.View;
import android.widget.Button;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.util.regex.Pattern;

/**
 * Finish Setup page, which should be launched after DO or PO provisioning is successful.
 */
public final class FinishSetupPage extends UiPage {

    /**
     * Flag indicating if account was migrated.
     */
    private final boolean mIsAccountMigrated;

    /**
     * {@link BySelector} for TestDpc SuW layout title "Finish setup".
     */
    private static final BySelector TESTDPC_FINISH_SETUP_PAGE_FINISH_BUTTON_SELECTOR =
            By.pkg(TESTDPC_PKG_NAME)
                .clazz(Pattern.compile(Button.class.getName() + "|" + View.class.getName()))
                .text("FINISH")
                .enabled(true);

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     * @param isAccountMigrated whether account was migrated
     */
    public FinishSetupPage(UiDevice uiDevice, TestConfig config, boolean isAccountMigrated) {
        super(uiDevice, config);
        mIsAccountMigrated = isAccountMigrated;
    }

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public FinishSetupPage(UiDevice uiDevice, TestConfig config) {
        this(uiDevice, config, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return TESTDPC_FINISH_SETUP_PAGE_FINISH_BUTTON_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        if (mIsAccountMigrated) {
            // Find message that account migration was successful.
            BySelector succeedResult =
                    By.pkg(TESTDPC_PKG_NAME).text("Added account that is now managed:");

            // Find managed work account name to verify required account is migrated.
            BySelector managedAccount =
                    By.res(TESTDPC_PKG_NAME, "managed_account_name")
                            .text(getTestConfig().getWorkAccountUsername().toLowerCase());

            if (WidgetUtils.safeWait(getUiDevice(), succeedResult) == null ||
                    WidgetUtils.safeWait(getUiDevice(), managedAccount) == null) {
                throw new RuntimeException(String.format(
                        "Provisioning failed: %s is not setup to be managed by TestDpc!",
                         getTestConfig().getWorkAccountUsername()));
            }

            // Find page title to verify its text displays successful setup
            BySelector titleSelector = By.res(TESTDPC_PKG_NAME, "suw_layout_title");
            UiObject2 titleWidget = WidgetUtils.safeWait(getUiDevice(), titleSelector);
            if (titleWidget == null) {
                throw new RuntimeException("Provisioning failed: Could not find page title!");
            }
            if (!titleWidget.getText().equals("Finish setup")) {
                throw new RuntimeException(
                    String.format(
                        "Provisioning failed: Unexpected page title: %s",
                        titleWidget.getText()
                    ));
            }

            // Save Time metric
            getProvisioningStatsLogger().stopTime(STAT_TESTDPC_WORK_PROFILE_CREATION_TIME);
            getProvisioningStatsLogger().writeStatsToFile();

        } else {
            BySelector succeedResult = By.pkg(TESTDPC_PKG_NAME)
                    .text("To manage the new managed profile, visit the badged version of this");
            WidgetUtils.safeWait(getUiDevice(), succeedResult);
        }

        WidgetUtils.waitAndClick(getUiDevice(), TESTDPC_NEXT_BUTTON_SELECTOR);
    }
}
