/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.managedprovisioning;

import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_PKG_NAME;
import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_SETUP_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.STAT_TESTDPC_WORK_PROFILE_CREATION_TIME;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Managed Provisioning terms and conditions page.
 */
public class TermsPage extends BasePage {

    private static final long DEFAULT_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(20);

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public TermsPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return MANAGED_PROVISIONING_SETUP_BUTTON_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        WidgetUtils.waitAndClick(getUiDevice(), MANAGED_PROVISIONING_SETUP_BUTTON_SELECTOR,
                DEFAULT_TIMEOUT_MS);

        onProvisioningStarted();

        // Wait for the provisioning to finish.
        if (!waitForProvisioningToFinish()) {
            throw new RuntimeException("PO Provisioning timeout");
        }
    }

    /**
     * Handles provisioning started event.
     */
    protected void onProvisioningStarted() throws Exception {
        // Default to be TestDpc provisioning
        getProvisioningStatsLogger().startTime(STAT_TESTDPC_WORK_PROFILE_CREATION_TIME);
    }
}
