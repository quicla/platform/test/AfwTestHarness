/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages;

import static com.android.afwtest.uiautomator.Constants.PROVISIONING_STATS_FILE;

import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.util.Log;

import com.android.afwtest.common.Timer;
import com.android.afwtest.common.test.StatsLogger;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.test.AfwTestUiWatcher;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * Abstract class that represents the screen view of the device.
 */
public abstract class UiPage {

    private static final String TAG = "afwtest.UiPage";

    /**
     * Default waiting for a {@link UiPage}.
     */
    private static final long DEFAULT_LOAD_TIMEOUT_MS = TimeUnit.MINUTES.toMillis(1);

    /**
     * Reference to {@link UiDevice} object.
     */
    private final UiDevice mUiDevice;

    /**
     * Test configuration.
     */
    private final TestConfig mTestConfig;

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public UiPage(UiDevice uiDevice, TestConfig config) {
        mUiDevice = uiDevice;
        mTestConfig = config;
    }

    /**
     * Gets {@link UiDevice}.
     *
     * @return {@link UiDevice} object
     */
    protected UiDevice getUiDevice() {
        return mUiDevice;
    }

    /**
     * Gets test configuration.
     *
     * @return {@link TestConfig} object
     */
    protected TestConfig getTestConfig() {
        return mTestConfig;
    }

    /**
     * Gets the page loading timeout.
     *
     * @return page loading timeout in millisecond
     */
    protected long getLoadingTimeoutInMs() throws IOException {
        return DEFAULT_LOAD_TIMEOUT_MS * mTestConfig.getTimeoutSize();
    }

    /**
     * Gets if this page optional, e.g. might not appear in some flow.
     *
     * @return {@code true} if this page can be optional, {@code} false otherwise
     */
    public boolean isOptional() {
        return false;
    }

    /**
     * Waits for this page to load.
     *
     * @return {@code true} if this page is loaded successful, {@code false} otherwise
     */
    public boolean waitForLoading() throws Exception {
        Timer timer = new Timer(getLoadingTimeoutInMs());
        timer.start();
        do {
            if (WidgetUtils.safeWait(getUiDevice(), uniqueElement()) != null) {
                return true;
            }
            assertOnFatalAppCrash();
        } while (!timer.isTimeUp());

        Log.e(TAG, String.format("UiPage.waitForLoading timeout(%sms).", getLoadingTimeoutInMs()));

        return false;
    }

    /**
     * Asserts if fatal app crash is found.
     */
    protected void assertOnFatalAppCrash() {
        String appCrashMsg = AfwTestUiWatcher.getFatalAppCrashMsg();
        if (appCrashMsg != null) {
            throw new RuntimeException(appCrashMsg);
        }
    }

    /**
     * Gets provisioning stats logger.
     *
     * @return {@link StatsLogger} object
     */
    protected StatsLogger getProvisioningStatsLogger() throws IOException {
        return StatsLogger.getInstance(PROVISIONING_STATS_FILE);
    }

    /**
     * Gets unique element of this page described by {@link BySelector}.
     *
     * <p>The returned element is used to identify this page, such as to determine if this page
     * is visible. For example, {@link waitForLoading} function is using the returned element
     * of this function to determine if this page has successfully loaded.
     * </p>
     *
     * @return unique element of this page described by {@link BySelector}
     */
    public abstract BySelector uniqueElement();

    /**
     * Navigates this page properly to next page.
     *
     * @throws Exception if navigation failed
     */
    public abstract void navigate() throws Exception;
}
