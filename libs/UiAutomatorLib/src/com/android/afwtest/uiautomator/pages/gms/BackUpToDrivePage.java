package com.android.afwtest.uiautomator.pages.gms;

import static com.android.afwtest.uiautomator.Constants.GMS_BTN_WITH_TEXT_OK;
import static com.android.afwtest.uiautomator.Constants.GMS_PKG_NAME_REGEX;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.WidgetUtils;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


/**
 * Back up to Google Drive Page, optional.
 */
public final class BackUpToDrivePage extends UiPage {

    /**
     * Turn off back up to Google drive button.
     */
    private static final BySelector TURN_OFF_BACK_UP_BTN =
            By.res(Pattern.compile(GMS_PKG_NAME_REGEX + ":id/backup_opt_in_disable_backup",
                    Pattern.CASE_INSENSITIVE));

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public BackUpToDrivePage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return TURN_OFF_BACK_UP_BTN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected long getLoadingTimeoutInMs() {
        return TimeUnit.MINUTES.toMillis(1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isOptional() {
        // This page is optional.
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        WidgetUtils.waitAndClick(getUiDevice(), TURN_OFF_BACK_UP_BTN);
        WidgetUtils.waitToBeGone(getUiDevice(), TURN_OFF_BACK_UP_BTN);
        WidgetUtils.waitAndClick(getUiDevice(), GMS_BTN_WITH_TEXT_OK);
    }
}
