/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.gms;

import static com.android.afwtest.uiautomator.Constants.GMS_NEXT_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.GMS_PKG_NAME;
import static com.android.afwtest.uiautomator.Constants.GMS_TEXT_FIELD_SELECTOR;

import android.os.SystemClock;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.WidgetUtils;
import java.util.concurrent.TimeUnit;

/**
 * GMS Core auth enter password page.
 */
public final class EnterPasswordPage extends UiPage {

    /**
     * {@link BySelector} unique to this page.
     */
    private static final BySelector ENTER_PASSWORD_PAGE_SELECTOR =
        By.pkg(GMS_PKG_NAME).desc("Forgot password?");

    /**
     * Password of the account to add.
     */
    private final String mPassword;

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public EnterPasswordPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
        mPassword = config.getWorkAccountPassword();
    }

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     * @param customPasswordKey property key of the password in {@link TestConfig}
     */
    public EnterPasswordPage(UiDevice uiDevice, TestConfig config, String customPasswordKey) {
        super(uiDevice, config);
        mPassword = config.getProperty(customPasswordKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected long getLoadingTimeoutInMs() {
        // Accessing Google server takes long time
        return TimeUnit.MINUTES.toMillis(5);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return ENTER_PASSWORD_PAGE_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        WidgetUtils.waitAndClick(getUiDevice(),
                GMS_TEXT_FIELD_SELECTOR,
                TimeUnit.SECONDS.toMillis(5));
        // Clicking the text field will bring up the keyboard and change the view hierachy of
        // current screen; textField may become stale. Try to find it again before simulating
        // the keyboard events.
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(5));

        //Enter password by key event
        KeyCharacterMap map = KeyCharacterMap.load(KeyCharacterMap.VIRTUAL_KEYBOARD);
        KeyEvent[] events = map.getEvents(mPassword.toCharArray());
        for (KeyEvent event : events) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                getUiDevice().pressKeyCode(event.getKeyCode(), event.getMetaState());
            }
        }

        WidgetUtils.waitAndClick(getUiDevice(), GMS_NEXT_BUTTON_SELECTOR);
        // Wait until the next button is gone.
        WidgetUtils.waitToBeGone(getUiDevice(), GMS_NEXT_BUTTON_SELECTOR);
    }
}
