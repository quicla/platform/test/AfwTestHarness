/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.common.qrcodeprovisioning;

import static android.app.admin.DevicePolicyManager.ACTION_PROVISION_MANAGED_DEVICE_FROM_TRUSTED_SOURCE;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_ADMIN_EXTRAS_BUNDLE;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_COMPONENT_NAME;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_SIGNATURE_CHECKSUM;
import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_LEAVE_ALL_SYSTEM_APPS_ENABLED;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PersistableBundle;
import android.text.TextUtils;

import com.android.afwtest.common.test.TestConfig;
import java.io.IOException;
import java.util.Properties;

/**
 * Simulates QR code to start device owner provisioning.
 */
public final class QRCodeSimulator {

    /**
     * Sends QR code to initiate device owner provisioning.
     *
     * @param context {@link Context} object
     * @param propsConfigFile {@link Properties} configuration file containing QR code properties.
     */
    public static void sendQRCode(Context context, String propsConfigFile)
            throws IOException {
        TestConfig testConfig = TestConfig.get(propsConfigFile);
        // Fire an intent to start qrcode provisioning
        Intent provisioningIntent =
                new Intent(ACTION_PROVISION_MANAGED_DEVICE_FROM_TRUSTED_SOURCE);

        String deviceAdminPkgName = testConfig.getDeviceAdminPkgName();
        String deviceAdminReceiver = testConfig.getDeviceAdminReceiver();
        String deviceAdminPkgLocation = testConfig.getDeviceAdminPkgLocation();
        String deviceAdminPkgSignatureHash = testConfig.getDeviceAdminPkgSignatureHash();
        PersistableBundle adminExtras = testConfig.getAdminExtrasBundle();

        if (!TextUtils.isEmpty(deviceAdminPkgName) && !TextUtils.isEmpty(deviceAdminReceiver)) {
            provisioningIntent
                    .putExtra(EXTRA_PROVISIONING_DEVICE_ADMIN_COMPONENT_NAME,
                            new ComponentName(deviceAdminPkgName, deviceAdminReceiver));
        }
        if (!TextUtils.isEmpty(deviceAdminPkgLocation)) {
            provisioningIntent
                    .putExtra(EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION,
                    deviceAdminPkgLocation);
        }
        if (!TextUtils.isEmpty(deviceAdminPkgSignatureHash)) {
            provisioningIntent
                    .putExtra(EXTRA_PROVISIONING_DEVICE_ADMIN_SIGNATURE_CHECKSUM,
                            deviceAdminPkgSignatureHash);
        }
        provisioningIntent
                .putExtra(EXTRA_PROVISIONING_LEAVE_ALL_SYSTEM_APPS_ENABLED,
                        testConfig.getLeaveAllSystemAppsEnabled());
        if (adminExtras != null) {
            provisioningIntent.putExtra(EXTRA_PROVISIONING_ADMIN_EXTRAS_BUNDLE, adminExtras);
        }

        context.startActivity(provisioningIntent);
    }
}
