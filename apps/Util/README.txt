- About

  Common util app that works on user builds.

- Usage: connect a wifi

  adb shell am instrument -w -e action connect \
      -e ssid <ssid> -e security_type <security_type> -e password <password> \
      com.android.afwtest.util/.Wifi

  security_type could be WPA, WEP or NONE. Default to be NONE if no password is specified; default
  to be WPA if password is given.

- Usage: disconnect from wifi

  adb shell am instrument -w -e action disconnect com.android.afwtest.util/.Wifi

