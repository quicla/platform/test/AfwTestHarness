/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.systemutil;

import static com.android.afwtest.common.Constants.QR_CODE_FILE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.afwtest.common.qrcodeprovisioning.QRCodeSimulator;

import java.io.IOException;

/**
 * Activity to start QR code provisioning.
 */
public class StartQRCodeProvisioning extends Activity {

    private static final String TAG = "afwtest.StartQRCodeProvisioning";

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Intent intent = getIntent();
            if (intent.hasExtra(QR_CODE_FILE)) {
                QRCodeSimulator.sendQRCode(this, intent.getStringExtra(QR_CODE_FILE));
            } else {
                Log.e(TAG, String.format(
                        "QR code provisioning not started. Intent doesn't contain extra %s.",
                        QR_CODE_FILE));
            }
        } catch (IOException e) {
            Log.e(TAG, "Failed to start QR code provisioning", e);
        }
        finish();
    }
}
